# add the sublime text 2 ppa and grab key
apt_repository "sublime-text-2" do
  uri "http://ppa.launchpad.net/webupd8team/sublime-text-2/ubuntu/"
  distribution node['lsb']['codename']
  components ["main"]
  keyserver "keyserver.ubuntu.com"
  key "EEA14886"
end

# attempt to install sublime-text from package

package "sublime-text"