# Basic setup for a Linux Mint Developer Edition install

# all packages to install

terminal = %w{terminator}

packages = [ terminal ]

packages.flatten.each do |a_package|
  package a_package
end


# all recipes to include
shells = %w{zsh oh-my-zsh}
dvcs = %w{git}
apt = %w{apt}
sublime_text = %w{sublime-text-2}

recipes = [ shells, dvcs, apt, sublime_text ]

recipes.flatten.each do |a_recipe|
  include_recipe a_recipe
end